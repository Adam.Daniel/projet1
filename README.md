Résumé de travail :
1. Je fais la page personnage
2. Je fais la page accueil de cette Série TV
3. J'ai réussi à faire au moins deux pages de cette Série TV
4. J'ai lié ces deux pages avec la balise <a>

Explication :
+ Pour la page personnage
    ~ J'ai fait une balise <header>, une balise <main> (contient deux <section>), une balise <aside> (contient trois sections), et une balise <footer> qui contient une balise <p>
    ~ Pour positionner les éléments, il y a quatre balises qui deviennent en flex-container dans cette page
    ~ C'est le <body>, le <ul> dans la balise <header>, les deux sections dans la balise <main>, et la balise <section> qui se trouve dans un des balises <section> dans la balise <aside>
+ Pour la page personnage
    ~ J'ai fait une balise <header>, une balise <main> (contient trois <section>), une balise <aside> (contient un <h2> et deux <section>), et un <footer> qui contient une balise <p>
    ~ Pour positionner les éléments, il y a trois balises qui deviennent en flex-container dans cette page
    ~ C'est le <body>, le <ul> dans le <header>, le <div> dans un <div> dans la balise <main>

Les difficultés que j'ai rencontrées (HTML) :
- Dans la <section> de la balise <aside> pour la page Accueil, j'ai du mal à trouver une façon pour lister les descriptions de cette TV Série. 
    + J'utilise la balise <table> pour mettre en forme les informations et les descriptions
    + Je n'ai pas affiché toutes les bordures de cette table pour qu'elle semble simple
- Dans la balise <div> de la balise <main> pour la page Accueil, j'ai du mal à légender chaque image des acteurs avec leurs noms proprement
    + J'utilise la balise <figcaption> qui permet de représenter une légende ou une légende décrivant le reste du contenu de son élément parent <figure>
    + En conséquence, il faut que j'utilise la balise <figure> pour son parent parce que cette balise doit se situer dans une balise <figure>

Les difficultés que j'ai rencontrées (CSS) :
- J'utilise beaucoup de déclaration qui ne sont pas utiles. Si je les enlèves, ça ne change rien sur la page
    + Je vérifie plusieurs fois s'il y a les déclarations qui ont des ambiguïtés
    + Je vérifie s'il y a un changement sur la page à chaque fois je modifie quelque chose sur le CSS
- Pour les deux images d'acteur dans la page Personnage, je ne peux pas choisir une image particulière pour appliquer une ou plusiers déclarations. C'est parce que je souhaite ajouter une bordure du côté droite pour la première image, et une bordure du côté gauche pour la deuxième image
    + J'associe chaque photo à une identification
    + J'ajoute l'attribut pour chaque image
    + Je choisis une image particulière en appelant son id (#id)